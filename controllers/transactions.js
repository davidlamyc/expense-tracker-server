var loki = require('lokijs');
var uuid = require('uuid').v4;

db = new loki('db.json');
const transactions = db.addCollection('transactions');

exports.getTransactions = (req, res, next) => {
    try {
        // res.status(200).json({
        //     success: true,
        //     count: transactions.data.length,
        //     data: transactions.data
        // });
        res.status(200).json(transactions.data);
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: err
        });
    }
}

exports.addTransaction = (req, res, next) => {
    try {
        const { text, amount } = req.body;
        if (!text || !amount) {
            throw new Error('invalidRequest');
        }
        const transaction = { id: uuid(), text, amount }
        transactions.insert(transaction);
        // res.status(201).json({
        //     success: true,
        //     data: transaction
        // });
        res.status(201).json(transaction);
    } catch (err) {
        if (err.message === 'invalidRequest') {
            return res.status(400).json({
                success: false,
                error: 'Invalid Request.'
            });
        }
        return res.status(500).json({
            success: false,
            error: err
        });
    }
}

exports.deleteTransaction = (req, res, next) => {
    try {
        const transaction = transactions.findObject({id: req.params.id})
        if (!transaction) {
            return res.status(404).json({
                success: false,
                error: 'No transaction found.'
            });
        }
        transactions.findAndRemove({id: req.params.id})
        res.status(202).json({
            success: true
        });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: err
        });
    }
}
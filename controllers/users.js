var loki = require('lokijs');
var uuid = require('uuid').v4;
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var verifyToken = require('../_helpers/verifyToken');

db = new loki('db.json');
const users = db.addCollection('users');

exports.register = (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            throw new Error('invalidRequest');
        }

        var hashedPassword = bcrypt.hashSync(password, 8);
        const user = {
            email : email,
            password : hashedPassword
        }
  
        users.insert(user);
        
        const token = jwt.sign({ email }, 'secret', {
            expiresIn: 86400 // expires in 24 hours
        });
        res.status(200).send({ auth: true, token: token });
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: err
        });
    }
}

exports.login = (req, res, next) => {
    const token = req.headers.authorization;
    const user = users.findObject({email: req.body.email});
    console.log(user);
    if (!user) {
        return res.state(404).json({ error: 'User not found.'})
    }
    bcrypt.compare(req.body.password, user.password)
        .then(isMatch => {
            if (isMatch) {
                const token = jwt.sign({ email: req.body.email }, 'secret', {
                    expiresIn: 86400 // expires in 24 hours
                });
                res.status(200).send({ auth: true, token: token });
            } else {
                res.status(400).send({ error: 'Password incorrect.'});
            }
        })

}

exports.getUsers = (req, res, next) => {
    try {
        res.status(200).json(users.data);
    } catch (err) {
        return res.status(500).json({
            success: false,
            error: err
        });
    }
}
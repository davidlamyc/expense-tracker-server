const express = require('express');
const router = express.Router();

var verifyToken = require('../_helpers/verifyToken');
const { getTransactions, addTransaction, deleteTransaction } = require('../controllers/transactions');

router.get('/', verifyToken, getTransactions);

router
    .route('/')
    .post(addTransaction);

router
    .route('/:id')
    .delete(deleteTransaction);

module.exports = router;
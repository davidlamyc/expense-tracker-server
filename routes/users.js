const express = require('express');
const router = express.Router();
const { register, login, getUsers } = require('../controllers/users');

router
    .route('/register')
    .post(register);

router
    .route('/login')
    .post(login);

router
    .route('/')
    .get(getUsers);

module.exports = router;